OAuth2 Server
=============

Este bundle es la implementación de un servidor de autorización OAuth2.

El servidor implementa los cuatro flujos básicos de autorización (grant types),
y genera tokens normales y tokens JWT (encriptados y con información sobre el usuario).

Para la implementación hemos modificado y ampliado el bundle
[oauth2-server-bundle](https://github.com/bshaffer/oauth2-server-bundle), para 
que acepte la generación de tokens JWT y para resolver algunos problemas que tiene
(o que al menos yo he tenido al usarlo directamente).

Este bundle está construido sobre la librería [oauth2-server-php](http://bshaffer.github.io/oauth2-server-php-docs/).

La modificación consiste en:

1. Se ha definido dos nuevos servicios:
   `yuido.oauth2.storage.jwt` (Yuido\OAuth2ServerBundle\JwtStorage) y `yuido.oauth2.server` (Yuido\OAuth2ServerBundle\Server que incluye el 
   `yuido.oauth2.storage.jwt` como tipo de storage para la generación de token JWT)

2. Se ha definicio el servicio `yuido.oauth2.user_provider` que corrige un problema que detecté con 
   el que viene originalmente en el bundle.
   
3. Se han implementado controladores para generar tokens normales y JWT en los cuatro "grant types". 

4. Se ha implementado una API de pruebas para enseñar como funciona todo este tinglado. 


Se recomienda encarecidamente leerse la especificación oficial de OAuth2 y dejarse de rollos de buscar tutoriales.

https://tools.ietf.org/html/rfc6749

Y leer la documentación del bundle en `Resources/doc/index.rst`.
