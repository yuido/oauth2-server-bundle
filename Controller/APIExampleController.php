<?php

namespace Yuido\OAuth2ServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class APIExampleController extends Controller
{
    /**
     * This is called with an access token, details
     * about the access token are then returned.
     * Used for verification purposes.
     *
     * @Route("/resource", name="oauth2_server_resource")*
     */
    public function verifyAction()
    {
        $server = $this->get('yuido.oauth2.server');
        $server->setConfig('allow_implicit', true);

        if (!$server->verifyResourceRequest($this->get('oauth2.request'), $this->get('oauth2.response'))) {
            return $server->getResponse();
        }

        $tokenData = $server->getAccessTokenData($this->get('oauth2.request'), $this->get('oauth2.response'));

        return new JsonResponse($tokenData);
    }


    /**
     * Acción de muestra que simula el Resource Server
     *
     * @Route("/resource_with_jwt", name="oauth2_server_resource_with_jwt")
     */
    public function apiAction(\Symfony\Component\HttpFoundation\Request $request) {


        $jwt_access_token = $request->get('access_token');

        $separator = '.';

        if (2 !== substr_count($jwt_access_token, $separator)) {
            throw new \Exception("Incorrect access token format");
        }

        list($header, $payload, $signature) = explode($separator, $jwt_access_token);


        $decoded_signature = $this->urlSafeB64Decode($signature);


        // The header and payload are signed together
        $payload_to_verify = $header . $separator . $payload;

        // however you want to load your public key
        $public_key = file_get_contents($this->container->getParameter('public_key'));

        // default is SHA256
        //$verified = openssl_verify($payload_to_verify, $decoded_signature, $public_key, OPENSSL_ALGO_SHA256);
        $verified = openssl_verify($payload_to_verify, $decoded_signature, $public_key, defined('OPENSSL_ALGO_SHA256') ? OPENSSL_ALGO_SHA256 : 'sha256') === 1;

        if ($verified != 1) {
            throw new \Exception("Cannot verify signature");
        }

        $response = array(
            'header' => $header,
            'payload' => $payload,
            'signature' => $signature,
            'decoded' => $this->urlSafeB64Decode($payload)
        );

        return new JsonResponse($response);
    }

    public function urlSafeB64Decode($b64) {
        $b64 = str_replace(array('-', '_'), array('+', '/'), $b64);

        return base64_decode($b64);
    }

}