<?php

namespace Yuido\OAuth2ServerBundle\Controller;

use OAuth2\Server;
use OAuth2\Storage\JwtAccessToken;
use OAuth2\Storage\Memory;
use OAuth2\Storage\Pdo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class TokenController extends Controller {

    /**
     * This is called by the client app once the client has obtained
     * an authorization code from the Authorize Controller (@see OAuth2\ServerBundle\Controller\AuthorizeController).
     * returns a JSON-encoded Access Token or a JSON object with
     * "error" and "error_description" properties.
     *
     * @Route("/ytoken", name="oauth2_server_ytoken")
     */
    public function tokenAction()
    {
        $server = $this->get('yuido.oauth2.server');
        $server->setConfig('allow_implicit', true);

        return $server->handleTokenRequest($this->get('oauth2.request'), $this->get('oauth2.response'));
    }

    /**
     * @Route("/jwt_token", name="oauth2_server_jwt_token")
     */
    public function jwtTokenAction(Request $request) {

        $server = $this->get('yuido.oauth2.server');

        $server->setConfig('use_jwt_access_tokens', TRUE);
        $server->setConfig('allow_implicit', true);

        return $server->handleTokenRequest($this->get('oauth2.request'), $this->get('oauth2.response'));
    }
}
